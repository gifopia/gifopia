# Gifopia

This is a simple web application that allows users to search for, view, and upload GIFs using the Giphy API. The app supports several functionalities, including displaying trending GIFs, searching for GIFs by query, displaying GIF details, and uploading GIFs. Users can also view their uploaded GIFs, mark a GIF as their favorite, and view a random GIF if they have not selected a favorite yet.

## Table of Contents
- Requirements
- Installation
- Usage
- Contributing
- License

## Functions
- Displays Trending Gifs
- Searches Gifs
- Displays Gif Details
- Uploads Gif
- Displays Uploaded Gifs
- Favorite Gif
- Native DOM API for DOM manipulations.
- Uses Git to keep your source code and for team collaboration.
- Uses GitLab’s issue tracking system.
- Uses ESLint to write consistently styled code.
- Uses modules and they should be single-responsible.
- Documented code explicitly following the JSDoc standard.
- Uses correct naming and write clean, self-documenting code.
- Has a usable user interface.
- Uses the latest features of ECMAScript.

## Installation
To run this app locally, you will need to have Node.js and npm installed on your computer. Once you have cloned or downloaded the project, navigate to the root directory and run the following command:

npm install


This will install all of the necessary dependencies to run the app.

## Usage
To start the app, run the following command in the root directory:

npm start
npx live-server

This will start the app on http://localhost:3000.

## Contributing
If you would like to contribute to this project, feel free to submit a pull request. Please make sure to follow the general requirements listed above, and include tests for any new functionality you add.

## License
This project is licensed under the MIT License.