/**
 * Constants used in the application
 */

/**
 * Home: A constant string representing the home page
 * Type: string
 */
export const HOME = 'home';

/**
 * FAVORITES: A constant string representing the favorites page
 * Type: string
 */
export const FAVORITES = 'favorites';

/**
 * TRENDING: A constant string representing the trending page
 * Type: string
 */
export const TRENDING = 'trending';

/**
 * ABOUT: A constant string representing the about page
 * Type: string
 */
export const ABOUT = 'about';

/**
 * UPLOAD: A constant string representing the upload page
 * Type: string
 */
export const UPLOAD = 'upload';

/**
 * CONTAINER_SELECTOR: A constant string representing the container selector.
 * Type: string
 */
export const CONTAINER_SELECTOR = '#container';

/**
 * FULL_HEART: A constant string representing the full heart symbol.
 * Type: string
 */
export const FULL_HEART = '❤';

/**
 * EMPTY_HEART: A constant string representing the empty heart symbol.
 * Type: string
 */
export const EMPTY_HEART = '♡';

/**
 * NAVIGATION_LINK: A constant string representing the navigation link class name.
 * Type: string
 */
export const NAVIGATION_LINK = 'nav-link';

/**
 * DATA_PAGE: A constant string representing the data page attribute name.
 * Type: string
 */
export const DATA_PAGE = 'data-page';

/**
 * BTN_TRENDING_VIEW: A constant string representing the trending view button class name.
 * Type: string
 */
export const BTN_TRENDING_VIEW = 'btn-view-trending';

/**
 * DATA_ID: A constant string representing the data ID attribute name.
 * Type: string
 */
export const DATA_ID = 'data-id';

/**
 * BTN_UPLOAD_GIF: A constant string representing the upload GIF button class name.
 * Type: string
 */
export const BTN_UPLOAD_GIF = 'btn-upload-gif';

/**
 * SAVED_GIFS: A constant string representing the saved GIFs storage key.
 * Type: string
 */
export const SAVED_GIFS = 'savedGifs';

/**
 * UPLOADED_GIFS: A constant string representing the uploaded GIFs storage key.
 * Type: string
 */
export const UPLOADED_GIFS = 'uploaded-gifs';

/**
 * INPUT_SEARCH: A constant string representing the search input selector.
 * Type: string
 */
export const INPUT_SEARCH = 'input#search';
