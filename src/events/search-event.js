import { q } from './helpers.js';
import { toSearchView } from '../views/search-view.js';
import { CONTAINER_SELECTOR, HOME } from '../common/constants.js';
import { searchGifs } from '../request/request.js';
import { loadPage } from './nav-event.js';
import { INPUT_SEARCH } from '../common/constants.js';

/**
 * Render hte search result based on the given search text.
 * @param {string} searchText - The search text to query GIFs. 
 */
export const renderSearchItems = async (searchText) => {
  if (searchText.length < 1) {
    // Clear the container and load the home page if the search text is empty
    q(CONTAINER_SELECTOR).innerHTML = '';
    loadPage(HOME);
    return;
  }
  //
  const gif = await searchGifs(searchText);

  const searchView = toSearchView(gif, searchText);

  q(CONTAINER_SELECTOR).innerHTML = searchView;
};

export const clearTab = () => {
  q(INPUT_SEARCH).value = '';
  q(INPUT_SEARCH).placeholder = 'Search for gifs 🔎';
};
