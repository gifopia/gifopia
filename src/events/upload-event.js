
// Handle the upload form submission and save the uploaded GIF in local storage
import { uploadGif } from '../request/request.js';
import { renderUploadedGIFs } from '../views/upload-view.js';


export const uploadFormHandler = () => {
  console.log('Form submitted');
  // Get the form element, file input element, and message element
  const uploadForm = document.getElementById('upload-form');
  const fileInput = document.querySelector('input[type="file"]');
  const uploadMessage = document.getElementById('upload-message');

  // Add an event listener to the form's "submit" event
  uploadForm.addEventListener('submit', async (event) => {
    // Prevent the default form submission behavior
    event.preventDefault();
    console.log('Form submitted');

    // Retrieve the selected file from the file input element
    const file = fileInput.files[0];

    // If no file is selected, display an error message and return
    if (!file) {
      console.error('No file selected.');
      uploadMessage.innerText = 'No file selected!';
      return;
    }

    // Show the loading message
    uploadMessage.innerText = 'Uploading GIF...';

    // Upload the selected file using the `uploadGif` function
    const uploadedGif = await uploadGif(file);
    console.log(uploadedGif);

    // Show the success message
    uploadMessage.innerText = 'GIF uploaded successfully!';

    // refresh upload view to show uploaded GIF
    renderUploadedGIFs();
  });
};
