import { getFavorites, removeFavorite, addFavorite } from '../data/favorites.js';
import { q } from './helpers.js';
import { EMPTY_HEART, FULL_HEART } from '../common/constants.js';

/**
 * Toggle the favorite status of a GIF by adding or removing its ID from the favorites array, updating the localStorage data and changing the heart icon.
 * @param {string} gifId - The ID of the GIF to toggle the favorite status of 
 */
export const toggleFavoriteStatus = (gifId) => {
  const favorites = getFavorites();
  const heartSpan = q(`span[data-id="${gifId}"]`);

  if (favorites.includes(gifId)) {
    removeFavorite(gifId);
    heartSpan.classList.remove('active');
    heartSpan.innerHTML = EMPTY_HEART;
  } else {
    addFavorite(gifId);
    heartSpan.classList.add('active');
    heartSpan.innerHTML = FULL_HEART;
  }
};

/**
 * Render the heart icon for a GIF based on its favorite status
 * @param {string} gifId - The ID of the GIF to render the heart icon for. 
 * @returns {string} The HTML markup for the heart icon element.
 */
export const renderFavoriteStatus = (gifId) => {
  const favorites = getFavorites();

  return favorites.includes(gifId) ?
    `<span class="favorites active" data-id="${gifId}">${FULL_HEART}</span>` :
    `<span class="favorites" data-id="${gifId}">${EMPTY_HEART}</span>`;
};
