import { setActiveNav, q } from './helpers.js';
import { CONTAINER_SELECTOR, HOME, TRENDING, UPLOAD, FAVORITES, ABOUT } from '../common/constants.js';
import { toHomeView } from '../views/home-view.js';
import { toTrendingsView } from '../views/trending-view.js';
import { getTrending, getGIFById } from '../request/request.js';
import { apiKey } from '../common/api.js';
import { toGIFView } from '../views/gif-view.js';
import { toUploadView, renderUploadedGIFs } from '../views/upload-view.js';
import { getFavorites } from '../data/favorites.js';
import { toFavoritesView } from '../views/favorites-view.js';
import { toAboutView } from '../views/about-view.js';
import { getRandomGif } from '../request/request.js';
import { clearTab } from './search-event.js';

// public API

/**
 * Loads the specified page into the application container
 * @param {string} page - The page to load.  
 * @returns {null|Promise<void>} -  Returns null if an invalid page is specified; otherwise, returns a Promise that resolves when the page has finished rendering.
 */
export const loadPage = (page = '') => {


  switch (page) {

  case HOME:
    clearTab();
    setActiveNav(HOME);
    return renderHome();

  case TRENDING:
    clearTab();
    setActiveNav(TRENDING);
    return renderTrending();

  case FAVORITES:
    clearTab();
    setActiveNav(FAVORITES);
    return renderFavorites();

  case ABOUT:
    clearTab();
    setActiveNav(ABOUT);
    return renderAbout();

  case UPLOAD:
    clearTab();
    setActiveNav(UPLOAD);
    return renderUpload();
    /* if the app supports error logging, use default to log mapping errors */
  default: return null;
  }

};

/**
 * Renders the details for a GIF with the specified ID.
 * @async
 * @param {string} id - The ID of the GIF to render
 * @returns {Promise<void>} - Return a Promise that resolves when the GIF details have finished rendering 
 */
export const renderGIFDetails = async (id) => {
  const gif = await getGIFById(id);
  q(CONTAINER_SELECTOR).innerHTML = toGIFView(gif);
};

// private functions

/**
 * Render the home page into the application container
 */
const renderHome = () => {
  q(CONTAINER_SELECTOR).innerHTML = toHomeView();
};

/**
 * Renders the upload page into the application container.
 */
const renderUpload = () => {
  q(CONTAINER_SELECTOR).innerHTML = toUploadView();
  renderUploadedGIFs();

};

/**
 * Renders the about page into the application container.
 */
const renderAbout = () => {
  q(CONTAINER_SELECTOR).innerHTML = toAboutView();
};

/**
 * Renders the trending GIFs into the application container.
 *
 * @async
 * @returns {Promise<void>} - Returns a Promise that resolves when the trending GIFs have finished rendering.
 */
const renderTrending = async () => {
  const gifs = await getTrending(apiKey);
  q(CONTAINER_SELECTOR).innerHTML = toTrendingsView(gifs);
};

/**
 * Renders the user's favorite GIFs into the application container.
 *
 * @async
 * @returns {Promise<void>} - Returns a Promise that resolves when the favorite GIFs have finished rendering.
 */
const renderFavorites = async () => {
  const gifIds = getFavorites();
  const gifPromises = gifIds.map((id) => getGIFById(id));
  const gifs = await Promise.all(gifPromises);
  const randomGif = await getRandomGif();

  q(CONTAINER_SELECTOR).innerHTML =
    gifs.length === 0 ? randomGif : toFavoritesView(gifs);
};
