import { DATA_PAGE, HOME, NAVIGATION_LINK, BTN_TRENDING_VIEW, DATA_ID, BTN_UPLOAD_GIF, FAVORITES, INPUT_SEARCH } from './common/constants.js';
import { loadPage, renderGIFDetails } from './events/nav-event.js';
import { renderSearchItems } from './events/search-event.js';
import { q } from './events/helpers.js';
import { uploadFormHandler } from './events/upload-event.js';
import { toggleFavoriteStatus } from './events/favorite-event.js';

document.addEventListener('DOMContentLoaded', async () => {

  // add global listener
  document.addEventListener('click', event => {
    // nav events
    if (event.target.classList.contains(NAVIGATION_LINK)) {
      loadPage(event.target.getAttribute(DATA_PAGE));
    }
    // show trending event
    if (event.target.classList.contains(BTN_TRENDING_VIEW)) {
      renderGIFDetails(event.target.getAttribute(DATA_ID));
    }
    // upload event
    if (event.target.classList.contains(BTN_UPLOAD_GIF)) {
      uploadFormHandler();
    }
    // toggle favorite event 
    if (event.target.classList.contains(FAVORITES)) {
      toggleFavoriteStatus(event.target.getAttribute(DATA_ID));
    }
  });
  // search event
  q(INPUT_SEARCH).addEventListener('input', (event) => {
    renderSearchItems(event.target.value);
  });
  loadPage(HOME);
});
