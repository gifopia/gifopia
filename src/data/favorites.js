/**
 * Favorites is an array of GIF IDs that have been added as favorites
 * The array is initialized by parsing the JSON data stored in localStorage
 * Type: array
 */
export let favorites = JSON.parse(localStorage.getItem('favorites')) || [];

/**
 * Adds a GIF ID to the favorites array and updates the data stored in localStorage
 * 
 * @param {string} gifId - The ID of the GIF to add as a favorite 
 * @returns if the gifs has already been added to favorites
 */
export const addFavorite = (gifId) => {
  if (favorites.find(id => id === gifId)) {
    // Gifs has already been added to favorites
    return;
  }

  favorites.push(gifId);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * Remove a GIF ID from the favorites array and updates the data stored in localStorage
 * @param {string} gifId - The ID of the GIF to remove from favorites
 */
export const removeFavorite = (gifId) => {
  favorites = favorites.filter(id => id !== gifId);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * 
 * @returns Returns a copy of the favorites array
 */
export const getFavorites = () => [...favorites];
