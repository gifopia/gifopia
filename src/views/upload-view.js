import { SAVED_GIFS, UPLOADED_GIFS } from '../common/constants.js';
import { toSingleGifView } from '../views/trending-view.js';

/**
 * Returns the HTML markup for the Upload view.
 * @returns {string} The HTML markup for the Upload view.
 */
export const toUploadView = () => `
  <div id="Upload GIF">
    <h1 style="text-align: center; padding: 25px 0px;">Upload your own GIF!</h1>
    <div class="content">
      <form id="upload-form">
        <div class="form-group">
          <label for="gif-file">Choose a GIF file:</label>
          <input type="file" id="gif-file" name="gif-file" accept=".gif">
        </div>
        <button type="submit" class="btn-upload-gif">Upload GIF</button>
      </form>
      
    </div>
    <div id = "upload-message"></div>
    <div id="uploaded-gifs"></div>
  </div>
`;

/**
 * Renders the HTML markup for the uploaded GIFs.
 * @returns {void}
 */
export const renderUploadedGIFs = () => {
  const savedGifs = JSON.parse(localStorage.getItem(SAVED_GIFS)) || [];

  const html = savedGifs
    .map((gif) => toSingleGifView({ id: gif.id, url: gif.url, title: '' }))
    .join('');

  const container = document.getElementById(UPLOADED_GIFS);
  container.innerHTML = `
    <div id="uploaded-GIFs">
      <h1 style="text-align: center; padding: 25px 0px;">Your Uploaded GIFs</h1>
      <div class="content">
        ${html}
      </div>
    </div>
  `;
};


