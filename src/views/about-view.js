/**
 * Returns a string containing HTML markup for the "About" view.
 * @returns {string} HTML markup for the "About" view.
 */
export const toAboutView = () => `
<div id="about">
  <div><br>
    <center><h1>About GIFOPIA</h1><br> </center>
    <center><h2>Authors: Team 5 - Mariyan, Kamelia, Ivan</h2><br> </center>
    <center><h2>Year: 2023</h2><br> </center>
  </div>
</div>
`;
