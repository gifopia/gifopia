/**
 * Returns a string representing the home view of the GIF app.
 * The home view contains a simple app description and a list of available actions.
 *
 * @returns {string} The HTML string representing the home view.
 */
export const toHomeView = () => `
<div id="home">
<center> <h1>Welcome to GIFOPIA</h1></center>
  <div class="content">
    <p>GIFOPIA interacts with the Gify API:</p>
    <ul>
      <li>View Trending Gifs</li>
      <li>Search Gifs</li>
      <li>View Gif details</li>
      <li>Upload Gifs</li>
      <li>Add your favorite Gifs</li>
    </ul>
  </div>
  <center><img style="padding: 50px; width: 50vw;"src = "https://media2.giphy.com/media/OF0yOAufcWLfi/giphy.gif?cid=ecf05e477vwijmwepez1b3fah2ujpe0nk04m4f2k1bz9zh1z&rid=giphy.gif"></center>
</div>
`;
