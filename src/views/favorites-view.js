import { toSingleGifView } from './trending-view.js';

/**
 *  Returns the HTML content for the "favorite" view.
 * @param {Object[]} gifs - An array of GIF objects to be displayed. 
 * @returns {string} The HTML content for the view.
 */
export const toFavoritesView = (gifs) => `
<div id="gifs">
  <center><h1>Favorite gifs:</h1><center>
  <center><div class="content">
    ${gifs.map(toSingleGifView).join('\n') ||
    '<center><p>Add some gifs to favorites to see them here.</p></center>'}
  </div></center>
</div>
`;
