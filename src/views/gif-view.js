import { renderFavoriteStatus } from '../events/favorite-event.js';

/**
 * Returns the HTML code for the rendering a single GIF with its details. 
 * @param {Object} gif - The GIF object containing its details.
 * @returns {string} - The HTML code for render the single GIF with its details.
 */
export const toGIFView = (gif) => `
  <div class="gif-detail">
    <h1 style = "padding: 50px 0px;">${gif.title}</h1>
    <center><img src="${gif.url}" alt="${gif.title}" class = "singleGIFpreview"></center>
    <center><p>Rating: ${gif.rating} <br>
    Uploaded by: @${gif.username}</p></center>
    <center>${renderFavoriteStatus(gif.id)} </center>
  </div>
`;
