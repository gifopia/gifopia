import { toSingleGifView } from './trending-view.js';

/**
 * Renders a search view with a list of searched gifs or a message if no results found
 * @param {Array} gif - Array of gifs to be rendered
 * @param {String} searchTerm - The term used to search for the gifs
 * @returns {String} - HTML string representing the search view
 */
export const toSearchView = (gif, searchTerm) => `
<div id="gif">
  <h1>Search gifs "${searchTerm}":</h1>
  <div class="content">
    ${gif.map(toSingleGifView).join('\n') ||
    '<p>No gifs found</p>'}
  </div>
</div>
`;
