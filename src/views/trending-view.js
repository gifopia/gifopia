import { renderFavoriteStatus } from '../events/favorite-event.js';

/**
 * Returns the HTML string for the Trending GIFs view.
 * @param {Object[]} gifs - Array of objects containing GIF data.
 * @returns {string} - The HTML string for the Trending GIFs view.
 */
export const toTrendingsView = (gifs) => `
<div id="Trending GIFs">
  <h1 style = "text-align: center; padding: 25px 0px;">The best Trending GIFs right now!</h1>
  <div class="content">
    ${gifs.map(toSingleGifView).join('\n')}
  </div>
</div>
`;

/**
 * Returns the HTML string for a single trending GIF item.
 * @param {Object} gif - An object containing GIF data.
 * @returns {string} - The HTML string for a single trending GIF item.
 */
export const toSingleGifView = (gif) => {
  const words = gif.title.split(' ');
  const firstThreeWords = words.slice(0, 3).join(' ');
  return `
    <div class="content-single">
      <p style="text-align: center; padding: 0px 25px;">${firstThreeWords}</p>
      <img class="singleGIF" src="${gif.url}" alt="${gif.title}">
      <button class="btn-view-trending" data-id="${gif.id}">View GIF</button>
      ${renderFavoriteStatus(gif.id)}
    </div>
  `;
};
