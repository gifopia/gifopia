import { apiKey, endpoint } from '../common/api.js';
import { SAVED_GIFS } from '../common/constants.js';
import { toSingleGifView } from '../views/trending-view.js';

/**
 * Fetches the list of the trending GIFs form the Giphy API.
 * @param {string} apiKey - The Giphy API key.
 * @return {Object[]} - An array of GIF objects.
 */


export const getTrending = async (apiKey) => {

  const url = `${endpoint}/trending?api_key=${apiKey}`;

  try {
    const response = await fetch(url);
    const data = await response.json();
    return data.data.map(gif => ({
      id: gif.id,
      title: gif.title,
      url: gif.images.fixed_width.url,
    }));
  } catch (error) {
    console.error(error);
    return [];
  }
};

/**
 * Fetches a single GIF by ID from the Giphy API.
 * @param {string} gifId - The ID of the GIF to fetch.
 * @return {Object|null} - A GIF object or null if the GIF was not found.
 */
export const getGIFById = async (gifId) => {
  const url = `${endpoint}/${gifId}?api_key=${apiKey}`;

  try {
    const response = await fetch(url);
    const data = await response.json();
    const gif = data.data;
    return {
      id: gif.id,
      title: gif.title,
      url: gif.images.fixed_width.url,
      username: gif.username,
      source: gif.source,
      rating: gif.rating,
    };
  } catch (error) {
    console.error(error);
    return null;
  }
};


// request for GIF upload
/**
 * Upload a GIF file.
 * @param {File} file - The GIF file to upload.
 * @return {Object|null} - A GIF object or null if the upload failed.
 */
export const uploadGif = async (file) => {
  const endpointUpl = 'https://upload.giphy.com/v1/gifs';
  const url = `${endpointUpl}?api_key=${apiKey}`;
  const formData = new FormData();
  formData.append('file', file);
  try {
    const response = await fetch(url, {
      method: 'POST',
      body: formData,
    });
    const data = await response.json();
    const gif = data.data;

    // Save the GIF to local storage
    const savedGifs = JSON.parse(localStorage.getItem(SAVED_GIFS)) || [];
    const newGif = {
      id: gif.id,
      url: `https://media.giphy.com/media/${gif.id}/giphy.gif`,
    };
    savedGifs.push(newGif);
    localStorage.setItem('savedGifs', JSON.stringify(savedGifs));
    document.querySelector('#upload-form').reset();

    return newGif;
  } catch (error) {
    console.error(error);
    return null;
  }
};

/**
 * Searches for GIFs by a given search text.
 * @param {string} searchText - The text to search for.
 * @return {Object[]} - An array of GIF objects
 */
export const searchGifs = async (searchText) => {
  const url = `${endpoint}/search?api_key=${apiKey}&q=${searchText}`;

  try {
    const response = await fetch(url);
    const data = await response.json();
    return data.data.map(gif => ({
      id: gif.id,
      title: gif.title,
      url: gif.images.fixed_width.url,
    }));
  } catch (error) {
    console.error(error);
    return [];
  }
};

/**
 *
 * @return response from the api
 */
export const getRandomGif = async () => {
  const response = await fetch(`${endpoint}/random?api_key=${apiKey}`);
  const data = await response.json();
  const myGif = await getGIFById(data.data.id);

  return toSingleGifView(myGif);
};
